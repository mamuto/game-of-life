package com.almostleet.gameoflife;

import com.almostleet.gameoflife.GameLife;

public class Cell {
	
	private int row;
	private int col;
	private int status; // 0 is dead, 1 is alive

	public Cell(int row, int col, int status) {
		this.row = row;
		this.col = col;
		this.status = status;
	}

	public int getStatus(){
		return status;
	}
	
	public int nextIteration() {
		
		if ((status == 0) && (checkNeighbors() == 3)) { // If dead and exactly 3 neighbors, cell becomes alive
			status = 1;
			return status;
		}
		else if ((status == 1) && ((checkNeighbors() < 2) || (checkNeighbors() > 3))) { // If alive and either starved or overpop, cell dies
			status = 0;
			return status;
		}
		else {
			return status;
		}	
	}
	
	public int checkNeighbors() {
		int alive = 0;
		
			try {
				if (WorldCanvas.getGrid()[row-1][col-1].getStatus() == 1){
					alive += 1;
				}
				
			} catch (ArrayIndexOutOfBoundsException a) {
				
			} finally {
				
			}
			
		return alive;
	}
}
