package com.almostleet.gameoflife;

import java.text.SimpleDateFormat;
import java.util.*;
import java.awt.Point;
//import android.graphics.Point;



public class ClockWorld {

	final static int ROWS = 320;
	final static int COLS = 480;
	

	public ClockWorld() {
	}

	public static String getTime() {
		// Returns a string in the format "HHmm"
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		return sdf.format(System.currentTimeMillis());
	}
	
	public static int getRows() {
		return ROWS;
	}
	public static int getCols() {
		return COLS;
	}

	public static int[][] setBoard(String cur_time) {
	    // Sets live cells based on current time
		int[][] grid = new int[ROWS][COLS];
		for (int pos = 0; pos < 4; ++pos) {
			int i = Character.getNumericValue(cur_time.charAt(pos));
			HashSet<Point> coord_set = new HashSet<Point>();
			coord_set = premadeNumber(i);
			// To iterate over the Hashset, create itr
			Iterator<Point> itr = coord_set.iterator();
			while (itr.hasNext()) {
				Point coord = itr.next();
				// THIS NEEDS WORK \/
				if (pos == 0) {
					grid[coord.x+99][(coord.y+40)] = 1;
				}
				else if (pos == 1) {
					grid[coord.x+99][(coord.y+120)] = 1;
				}
				else if (pos == 2) {
					grid[coord.x+99][(coord.y+236)] = 1;
				}
				else if (pos == 3) {
					grid[coord.x+99][(coord.y+316)] = 1;
				}
			}
		}
		
		// double dots between hours and minutes
		for (int p = 0; p<16; ++p){
			for (int q = 0; q<16; ++q){
				grid[120+p][200+q] = 1;
				grid[170+p][200+q] = 1;
			}
		}
		
		return grid;
	}
 
	private static HashSet<Point> premadeNumber(int num) {
				
		// Creates and stores all digits in a Map // x is vertical, y is horizontal
		// Zero
		HashSet<Point> THB = new HashSet<Point>();
		// Top Horizontal Bar
		for (int val = 5; val < 56; ++val) {
			THB.add(new Point(0, val));}
		for (int val = 6; val < 55; ++val) {
			THB.add(new Point(1, val));}
		for (int val = 7; val < 54; ++val) {
			THB.add(new Point(2, val));}
		for (int val = 8; val < 53; ++val) {
			THB.add(new Point(3, val));}
		for (int val = 9; val < 52; ++val) {
			THB.add(new Point(4, val));}
		for (int val = 10; val < 51; ++val){
			THB.add(new Point(5, val));}
		for (int val = 11; val < 50; ++val){
			THB.add(new Point(6, val));}
		for (int val = 12; val < 49; ++val){
			THB.add(new Point(6, val));}
		for (int val = 13; val < 48; ++val){
			THB.add(new Point(7, val));}
		for (int val = 14; val < 47; ++val){
			THB.add(new Point(8, val));}
		for (int val = 15; val < 46; ++val){
			THB.add(new Point(9, val));}
		
		HashSet<Point> TLV = new HashSet<Point>();
		// Top Left Vertical Bar
		for (int val = 5; val < 46; ++val) {
			TLV.add(new Point(val, 0));}
		for (int val = 6; val < 47; ++val) {
			TLV.add(new Point(val, 1));}
		for (int val = 7; val < 48; ++val) {
			TLV.add(new Point(val, 2));}
		for (int val = 8; val < 49; ++val) {
			TLV.add(new Point(val, 3));}
		for (int val = 9; val < 50; ++val) {
			TLV.add(new Point(val, 4));}
		for (int val = 10; val < 49; ++val){
			TLV.add(new Point(val, 5));}
		for (int val = 11; val < 48; ++val){
			TLV.add(new Point(val, 6));}
		for (int val = 12; val < 47; ++val){
			TLV.add(new Point(val, 7));}
		for (int val = 13; val < 46; ++val){
			TLV.add(new Point(val, 8));}
		for (int val = 14; val < 45; ++val){
			TLV.add(new Point(val, 9));}
		
		HashSet<Point> TRV = new HashSet<Point>();

		// Top Right Vertical Bar
		for (int val = 14; val < 45; ++val) {
			TRV.add(new Point(val, 50));}
		for (int val = 13; val < 46; ++val) {
			TRV.add(new Point(val, 51));}
		for (int val = 12; val < 47; ++val) {
			TRV.add(new Point(val, 52));}
		for (int val = 11; val < 48; ++val) {
			TRV.add(new Point(val, 53));}
		for (int val = 10; val < 49; ++val) {
			TRV.add(new Point(val, 54));}
		for (int val = 9; val < 50; ++val)  {
			TRV.add(new Point(val, 55));}
		for (int val = 8; val < 49; ++val)  {
			TRV.add(new Point(val, 56));}
		for (int val = 7; val < 48; ++val)  {
			TRV.add(new Point(val, 57));}
		for (int val = 6; val < 47; ++val)  {
			TRV.add(new Point(val, 58));}
		for (int val = 5; val < 46; ++val)  {
			TRV.add(new Point(val, 59));}

		HashSet<Point> MHB = new HashSet<Point>();
		// Middle Horizontal Bar
		for (int val = 14; val < 45; ++val) {
			MHB.add(new Point(50, val));}
		for (int val = 13; val < 46; ++val) {
			MHB.add(new Point(51, val));}
		for (int val = 12; val < 47; ++val) {
			MHB.add(new Point(52, val));}
		for (int val = 11; val < 48; ++val) {
			MHB.add(new Point(53, val));}
		for (int val = 10; val < 49; ++val) {
			MHB.add(new Point(54, val));}
		for (int val = 9; val < 50; ++val)  {
			MHB.add(new Point(55, val));}
		for (int val = 10; val < 49; ++val) {
			MHB.add(new Point(56, val));}
		for (int val = 11; val < 48; ++val) {
			MHB.add(new Point(57, val));}
		for (int val = 12; val < 47; ++val) {
			MHB.add(new Point(58, val));}
		for (int val = 13; val < 46; ++val) {
			MHB.add(new Point(58, val));}
		for (int val = 14; val < 45; ++val) {
			MHB.add(new Point(59, val));}
		
		
		HashSet<Point> BLV = new HashSet<Point>();		
		// Bottom Left Vertical Bar
		for (int val = 64; val < 105; ++val){
			BLV.add(new Point(val, 0));}
		for (int val = 63; val < 104; ++val){
			BLV.add(new Point(val, 1));}
		for (int val = 62; val < 103; ++val){
			BLV.add(new Point(val, 2));}
		for (int val = 61; val < 102; ++val){
			BLV.add(new Point(val, 3));}
		for (int val = 60; val < 101; ++val){
			BLV.add(new Point(val, 4));}
		for (int val = 61; val < 100; ++val){
			BLV.add(new Point(val, 5));}
		for (int val = 62; val < 99; ++val){
			BLV.add(new Point(val, 6));}
		for (int val = 63; val < 98; ++val){
			BLV.add(new Point(val, 7));}
		for (int val = 64; val < 97; ++val){
			BLV.add(new Point(val, 8));}
		for (int val = 65; val < 96; ++val){
			BLV.add(new Point(val, 9));}
		
		HashSet<Point> BRV = new HashSet<Point>();
		// Bottom Right Vertical Bar
		for (int val = 65; val < 96; ++val) {
			BRV.add(new Point(val, 50));}
		for (int val = 64; val < 97; ++val) {
			BRV.add(new Point(val, 51));}
		for (int val = 63; val < 98; ++val) {
			BRV.add(new Point(val, 52));}
		for (int val = 62; val < 99; ++val) {
			BRV.add(new Point(val, 53));}
		for (int val = 61; val < 100; ++val){
			BRV.add(new Point(val, 54));}
		for (int val = 60; val < 101; ++val){
			BRV.add(new Point(val, 55));}
		for (int val = 61; val < 102; ++val){
			BRV.add(new Point(val, 56));}
		for (int val = 62; val < 103; ++val){
			BRV.add(new Point(val, 57));}
		for (int val = 63; val < 104; ++val){
			BRV.add(new Point(val, 58));}
		for (int val = 64; val < 105; ++val){
			BRV.add(new Point(val, 59));}
		
		// Bottom Horizontal Bar
		HashSet<Point> BHB = new HashSet<Point>();
		for (int val = 5; val < 56; ++val) {
			BHB.add(new Point(111, val));}
		for (int val = 6; val < 55; ++val) {
			BHB.add(new Point(110, val));}
		for (int val = 7; val < 54; ++val) {
			BHB.add(new Point(109, val));}
		for (int val = 8; val < 53; ++val) {
			BHB.add(new Point(108, val));}
		for (int val = 9; val < 52; ++val) {
			BHB.add(new Point(107, val));}
		for (int val = 10; val < 51; ++val){
			BHB.add(new Point(106, val));}
		for (int val = 11; val < 50; ++val){
			BHB.add(new Point(105, val));}
		for (int val = 12; val < 49; ++val){
			BHB.add(new Point(104, val));}
		for (int val = 13; val < 48; ++val){
			BHB.add(new Point(103, val));}
		for (int val = 14; val < 47; ++val){
			BHB.add(new Point(102, val));}
		for (int val = 15; val < 46; ++val){
			BHB.add(new Point(101, val));}

		
		HashSet<Point> coord = new HashSet<Point>();
		if (num == 1) {
			coord.addAll(TRV);
			coord.addAll(BRV);
			return coord;
		}
		else if (num == 2) {
			coord.addAll(THB);
			coord.addAll(TRV);
			coord.addAll(MHB);
			coord.addAll(BLV);
			coord.addAll(BHB);
			return coord;
		}
		else if (num == 3) {
			coord.addAll(THB);
			coord.addAll(TRV);
			coord.addAll(MHB);
			coord.addAll(BRV);
			coord.addAll(BHB);
			return coord;
		}
		else if (num == 4) {
			coord.addAll(TLV);
			coord.addAll(TRV);
			coord.addAll(MHB);
			coord.addAll(BRV);
			return coord;
		}
		else if (num == 5) {
			coord.addAll(THB);
			coord.addAll(TLV);
			coord.addAll(MHB);
			coord.addAll(BRV);
			coord.addAll(BHB);
			return coord;
		}
		else if (num == 6) {
			coord.addAll(THB);
			coord.addAll(TLV);
			coord.addAll(MHB);
			coord.addAll(BLV);
			coord.addAll(BRV);
			coord.addAll(BHB);
			return coord;
		}
		else if (num == 7) {
			coord.addAll(THB);
			coord.addAll(TRV);
			coord.addAll(BRV);
			return coord;
		}
		else if (num == 8) {
			coord.addAll(THB);
			coord.addAll(TLV);
			coord.addAll(TRV);
			coord.addAll(MHB);
			coord.addAll(BLV);
			coord.addAll(BRV);
			coord.addAll(BHB);
			return coord;
		}
		else if (num == 9) {
			coord.addAll(THB);
			coord.addAll(TLV);
			coord.addAll(TRV);
			coord.addAll(MHB);
			coord.addAll(BRV);
			coord.addAll(BHB);
			return coord;
		}
		else {
			coord.addAll(THB);
			coord.addAll(TLV);
			coord.addAll(TRV);
			coord.addAll(BLV);
			coord.addAll(BRV);
			coord.addAll(BHB);
			return coord;
		}
	}
}