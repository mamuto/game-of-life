package com.almostleet.gameoflife.Screens;

import com.almostleet.gameoflife.GameLife;
import com.almostleet.gameoflife.ClockWorld;
import com.almostleet.gameoflife.Screens.SplashScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class ClockScreen extends AbstractScreen {
	
	ShapeRenderer batch;
	OrthographicCamera camera;
	boolean flag = true;
	int[][] grid;
	int[][] grid2 = new int[ClockWorld.getRows()][ClockWorld.getCols()];
	int alive;
	int coord_count;
	int[] result = new int[(ClockWorld.getCols()*ClockWorld.getRows())];
	int DELAY = 75;
	
	public ClockScreen (GameLife game){
		super(game);
	}

	@Override
	public void show() {
		super.show();
	    batch = new ShapeRenderer();
	    camera = new OrthographicCamera();
	    camera.setToOrtho(true, 720, 480);
	    grid = ClockWorld.setBoard(ClockWorld.getTime());
	}

	@Override
	public void render(float delta) {
		//super.render(delta);
		try {
			//Thread.sleep(DELAY);
			Thread.sleep(25);
			}
		catch (InterruptedException e) {
			e.printStackTrace();
		}

		
		calculateNext();
		
		Gdx.gl.glClearColor(0.f, 0.f, 0.f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		batch.begin(ShapeType.FilledRectangle);

		// Render cells code:
		for (int i = 0; i < coord_count; i += 2) {
			batch.filledRect((float)(result[i+1]*1.5), (float)(result[i]*1.5), 1.5f, 1.5f, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN);
		}
		batch.end();
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			dispose();
			Gdx.app.exit();
			//game.setScreen(new MainMenu(game));
			}
		
		if (Gdx.input.isKeyPressed(Keys.BACK)){
			dispose();
			Gdx.app.exit();
			}
	
		if (Gdx.input.isTouched()) {
			dispose();
			//game.setScreen(new StaticScreen(game));
			game.setScreen(game.staticScreen);
			}
	}
	
	private void calculateNext() {
		// Returns next generation/step of the grid
		if (flag) {
			coord_count = 0;
			flag = false;
			for (int row = 1; row < ClockWorld.getRows()-1; ++row) {
				for (int col = 1; col < ClockWorld.getCols()-1; ++col) {
					alive = 0;
					alive = grid[row-1][col-1] + grid[row-1][col] + grid[row-1][col+1] + grid[row][col-1] + 
							grid[row][col+1] + grid[row+1][col-1] + grid[row+1][col] + grid[row+1][col+1];

			    	// Decides fate of cell
					if ((grid[row][col] == 0) && (alive == 3)) { // If dead and exactly 3 neighbors, cell becomes alive
						grid2[row][col] = 1;
						result[coord_count] = row;
						result[coord_count+1] = col;
						coord_count += 2;
						}
					else if ((grid[row][col] == 1) && ((alive < 2) || (alive > 3))) { // If alive and either starved or overpop, cell dies
						grid2[row][col] = 0;
						}
					else if ((grid[row][col] == 1) && ((alive == 2) || (alive == 3))) { // Else, remains the same
						grid2[row][col] = 1;
						result[coord_count] = row;
						result[coord_count+1] = col;
						coord_count += 2;
						}
					else {
						grid2[row][col] = 0;
						}
					}
				}
			}

		else {
			flag = true;
			coord_count = 0;
			for (int row = 1; row < ClockWorld.getRows()-1; ++row) {
				for (int col = 1; col < ClockWorld.getCols()-1; ++col) {
					
					// Counts neighbors that are alive
					int alive = 0;
					alive = grid2[row-1][col-1] + grid2[row-1][col] + grid2[row-1][col+1] + grid2[row][col-1] +
							grid2[row][col+1] + grid2[row+1][col-1] + grid2[row+1][col] + grid2[row+1][col+1];
					
					// Decides fate of cell
					if ((grid2[row][col] == 0) && (alive == 3)) { // If dead and exactly 3 neighbors, cell becomes alive
						grid[row][col] = 1;
						result[coord_count] = row;
						result[coord_count+1] = col;
						coord_count += 2;
						}
					else if ((grid2[row][col] == 1) && ((alive < 2) || (alive > 3))) { // If alive and either starved or overpop, cell dies
						grid[row][col] = 0;
						}
					else if ((grid2[row][col] == 1) && ((alive == 2) || (alive == 3))) { // Else, remains the same
						grid[row][col] = 1;
						result[coord_count] = row;
						result[coord_count+1] = col;
						coord_count += 2;
						}
					else {
						grid[row][col] = 0;
						}
					}
				}
			}
	}
	
	@Override
	public void dispose() {
		super.dispose();
		batch.dispose();
	}
}
