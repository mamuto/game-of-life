package com.almostleet.gameoflife.Screens;

import com.almostleet.gameoflife.GameLife;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.almostleet.gameoflife.ClockWorld;

public class StaticScreen extends AbstractScreen {

	SpriteBatch batch = new SpriteBatch();
	OrthographicCamera camera;
	Pixmap pixmap = new Pixmap (720, 480, Format.RGBA8888);
	Texture pixmaptex;
	int[][] grid;
	boolean flag = true;
	float elapsedTime;
	
	public StaticScreen(GameLife game) {
		super(game);
	}
	
	@Override
	public void show() {
		super.show();
	    grid = ClockWorld.setBoard(ClockWorld.getTime());

	    int counter = 0;
	    camera = new OrthographicCamera();
	    camera.setToOrtho(true, 720, 480);

		Gdx.gl.glClearColor(0.f, 0.f, 0.f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		pixmap.setColor(Color.GREEN);
		for (int col = 0; col < ClockWorld.getCols(); ++col) {
			for (int row = 0; row < ClockWorld.getRows(); ++row) {
				if(grid[row][col] == 1) {
					counter++;
					pixmap.fillRectangle(col, row, 2, 2);
				}
			}
		}
		System.out.println("counter says: " +counter);
		pixmaptex = new Texture(pixmap);
		//pixmap.dispose();
		

	}
	
	public void render(float delta) {
		//super.render(delta);
		if (flag) {
			Gdx.gl.glClearColor(0.f, 0.f, 0.f, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
			batch.setProjectionMatrix(camera.combined);
			batch.begin();
			//batch.draw(pixmaptex, 0, 0, 720, 480, 0, 0, 720, 480, false, true);
			batch.draw(pixmaptex, 0, 0, 1140, 960, 0, 0, 720, 480, false, true);
			batch.end();
			elapsedTime = elapsedTime + delta;
			if (elapsedTime > 2) {
				flag = false;
			}
		}
		else {
			//dispose();
			game.setScreen(new ClockScreen(game));
		}
		
		//game.setScreen(new ClockScreen(game));
		
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			dispose();
			Gdx.app.exit();
			//game.setScreen(new MainMenu(game));
		}

		if (Gdx.input.isKeyPressed(Keys.BACK)){
			dispose();
			Gdx.app.exit();
		}

		if (Gdx.input.isTouched()) {
			dispose();
			game.setScreen(new StaticScreen(game));
		}
	}
}