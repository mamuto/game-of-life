package com.almostleet.gameoflife.Screens;

import com.almostleet.gameoflife.GameLife;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class SplashScreen extends AbstractScreen {
	
	private Image splashImage;
	
	public SplashScreen(GameLife game){
		super(game);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		if (Gdx.input.justTouched()) {
			Gdx.app.log(GameLife.LOG, "Clicked");
            //game.setScreen(new ClockScreen(game));
			game.setScreen(game.staticScreen);
		}
	}

	@Override
	public void show() {
		super.show();
		
		AtlasRegion splashRegion = getAtlas().findRegion("splash");
		Drawable splashDraw = new TextureRegionDrawable(splashRegion);
		
		splashImage = new Image(splashDraw, Scaling.stretch);
		splashImage.setFillParent(true);
		splashImage.getColor().a = 0f;

		splashImage.addAction(sequence(fadeIn(0.75f),delay(1.75f), fadeOut(0.75f),
				new Action() {
					@Override
					public boolean act(float delta) {
						Gdx.app.log(GameLife.LOG, "Tween Complete");
						dispose();
						game.setScreen(game.staticScreen);
						return true;
					}
				}
		));
		stage.addActor(splashImage);
	}
}