package com.almostleet.gameoflife.Screens;

import com.almostleet.gameoflife.GameLife;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class AbstractScreen implements Screen {
	
    protected final GameLife game;
    protected final Stage stage;
    
    private TextureAtlas atlas;
    private BitmapFont font;
    private SpriteBatch batch;
    private Skin skin;
	private Table table;
    
    public AbstractScreen(GameLife game) {
            this.game = game;
            this.stage = new Stage(720, 480, true);
        }
    
    protected String getName() {
    	return getClass().getSimpleName();
    }
    
    // Lazily loaded collaborators
    public TextureAtlas getAtlas() {
    	if (atlas == null) {
    		atlas = new TextureAtlas(Gdx.files.internal("data/golpack.pack"));
    	}
    	return atlas;
    }
    
    public BitmapFont getFont() {
    	if (font == null) {
    		font = new BitmapFont();
    	}
    	return font;
    }

    public SpriteBatch getBatch() {
    	if (batch == null) {
    		batch = new SpriteBatch();
    	}
    	return batch;
    }
    
    public Skin getSkin() {
    	if (skin == null) {
            //skin = new Skin(Gdx.files.internal("data/skin.json"));
    		skin = new Skin();
    	}
    	return skin;
    }

    protected Table getTable() {
    	if (table == null) {
    		table = new Table(getSkin());
    		table.setFillParent(true);
    		if (GameLife.DEV_MODE) {
    			table.debug();
    		}
    		stage.addActor(table);
    	}
    	return table;
    }
    
    
    // Screen methods
	@Override
	public void render(float delta) {

		// Clear screen to black
        Gdx.gl.glClearColor(0.f, 0.f, 0.f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		// Update logic
        //stage.act(delta);   
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		
		// Render frame
        stage.draw();
        // Table debug lines
        Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
        Gdx.app.log(GameLife.LOG, "Resizing screen: " + getName() + " to: " + width + " x " + height);

        // Resize the stage
        stage.setViewport(width, height, true);
	}

	@Override
	public void show() {
		Gdx.app.log(GameLife.LOG, "Showing screen: " + getName());
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void hide() {
		Gdx.app.log(GameLife.LOG, "Hiding/disposing screen: " + getName());
		//dispose();	
	}

	@Override
	public void pause() {
		Gdx.app.log(GameLife.LOG, "Pausing screen: " + getName());
		Gdx.app.exit();
	}

	@Override
	public void resume() {		
	}

	@Override
	public void dispose() {
		//stage.clear();
		// Lazy instantiation, they may be null
		if (font != null) font.dispose();
		if (batch != null) batch.dispose();
		if (atlas != null) atlas.dispose();
		if (skin != null) skin.dispose();
	}
}
