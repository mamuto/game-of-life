package com.almostleet.gameoflife;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.almostleet.gameoflife.Screens.*;
import com.badlogic.gdx.graphics.FPSLogger;




public class GameLife extends Game {
	
	public static final String VERSION = "0.5";
	public static final String LOG = "Game of Time";
	public static final boolean DEV_MODE = true;
	private FPSLogger fpsLogger;
    public SplashScreen splashScreen;
    public StaticScreen staticScreen;
    public ClockScreen clockScreen;
	
    public SplashScreen getSplashScreen() {
        return new SplashScreen(this);
    }
	
	@Override
	public void create() {
        Gdx.app.log(LOG, "Creating game");
        fpsLogger = new FPSLogger();
        staticScreen = new StaticScreen(this);
        clockScreen = new ClockScreen(this);
        splashScreen = new SplashScreen(this);
        
        setScreen(splashScreen);
		//setScreen(getSplashScreen());
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render() {
		super.render();
		fpsLogger.log();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}
}
