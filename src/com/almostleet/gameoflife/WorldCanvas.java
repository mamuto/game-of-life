package com.almostleet.gameoflife;

import com.almostleet.gameoflife.Cell;

public class WorldCanvas {
	
	protected int rows, columns;
	protected int blockWidth, blockHeight;
	int[] string; 
	private static Cell[][] grid;
	

	public WorldCanvas(int rows, int columns, int blockWidth, int blockHeight) {
		this.rows = rows;
		this.columns = columns;
		this.blockHeight = blockHeight;
		this.blockWidth = blockWidth;
		grid = new Cell[rows][columns];
	    for (int row = 0; row < rows; row++) {
	    	for (int col = 0; col < columns; col++) {
	    		grid[row][col] = new Cell(row,col,0);
	    	}
	    }
	}
	
	public static Cell[][] getGrid(){
		return grid;
	}
	
	public void printMe(){
		System.out.print(grid[0][1]);
    for (int row = 0; row < rows; row++) {
    	for (int col = 0; col < columns; col++) {
    		System.out.print(grid[row][col].checkNeighbors());
    	}
    }
    }
}